﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SatifyApi.Models;
using SatifyApi.Services;

namespace SatifyApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly SatifyDbContext _context;
        private readonly ITokenService _tokenService;

        public TokenController(SatifyDbContext context, ITokenService tokenService)
        {
            _context = context;
            _tokenService = tokenService;
        }

        [HttpPost]
        [Route("refresh")]
        public IActionResult Refresh(TokenApiModel tokenApiModel)
        {
            try
            {
                if (tokenApiModel is null)
                    return BadRequest("Invalid client request");
                string? accessToken = tokenApiModel.AccessToken;
                string? refreshToken = tokenApiModel.RefreshToken;
                if (!string.IsNullOrEmpty(accessToken))
                {
                    System.Security.Claims.ClaimsPrincipal? principal = _tokenService.GetPrincipalFromExpiredToken(accessToken);
                    string? username = principal.Identity?.Name;
                    LoginModel? user = _context.LoginModels?.SingleOrDefault(u => u.UserName == username);
                    if (user is null || user.RefreshToken != refreshToken || user.RefreshTokenExpiryTime <= DateTime.Now)
                        return BadRequest("Invalid client request");
                    string newAccessToken = _tokenService.GenerateAccessToken(principal.Claims);
                    string newRefreshToken = _tokenService.GenerateRefreshToken();
                    user.RefreshToken = newRefreshToken;
                    _context.SaveChanges();
                    return Ok(new AuthenticatedResponse()
                    {
                        Token = newAccessToken,
                        RefreshToken = newRefreshToken
                    });
                }
                else
                {
                    throw new Exception("Access token not passed.");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        [HttpPost, Authorize]
        [Route("revoke")]
        public IActionResult Revoke()
        {
            string? username = User.Identity?.Name;
            LoginModel? user = _context.LoginModels?.SingleOrDefault(u => u.UserName == username);
            if (user == null) return BadRequest();
            user.RefreshToken = null;
            _context.SaveChanges();
            return NoContent();
        }
    }
}
