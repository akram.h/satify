﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using SatifyApi.DTOs;
using SatifyApi.Models;
using SatifyApi.Services;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Twilio;
using Twilio.Rest.Verify.V2;
using Twilio.Rest.Verify.V2.Service;

namespace SatifyApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly SatifyDbContext _context;
        private readonly ITokenService _tokenService;
        private readonly string accountSid = "AC04069813d572dbc14524c5a77f764dd4";
        private readonly string authToken = "553090bb6ea82b3ee7dda6af52cee18a";

        public AuthController(SatifyDbContext context, ITokenService tokenService)
        {
            _context = context;
            _tokenService = tokenService;
        }

        [HttpPost, Route("Login")]
        public async Task<IActionResult> Login([FromBody] LoginDto login)
        {
            try
            {
                if (login is null)
                {
                    return BadRequest("Invalid client request");
                }
                //User? user = await _context.Users.FirstOrDefaultAsync(x => x.MobileNumber == login.MobileNumber);
                //if(user == null)
                //{
                //    return NotFound("User not found");  
                //}
                //TwilioClient.Init(accountSid, authToken);
                //var service = ServiceResource.Create(friendlyName: "Satify Login");
                //var verification = VerificationResource.Create(
                //    to: "+91" + login.MobileNumber,
                //    channel: "sms",
                //    pathServiceSid: service.Sid
                //);

                //user.UserSid = service.Sid;
                //_context.Entry(user).State = EntityState.Modified;
                //await _context.SaveChangesAsync();

                return Ok("OTP sent to your Mobile Number.");
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody] RegisterDto register)
        {
            try
            {
                User? existingUser = await _context.Users.FirstOrDefaultAsync(x => x.MobileNumber == register.MobileNumber);
                if(existingUser != null)
                {
                    return Ok("User exists. Please Login with your Mobile Number.");
                }
                User newUser = new User()
                {
                    UserName = register.UserName,
                    MobileNumber = register.MobileNumber,
                    Address = register.Address,
                    UserRole = "User",
                    CreatedOn = DateTime.Now,
                };
                await _context.AddAsync(newUser);
                await _context.SaveChangesAsync();

                return Ok("Registered successfully.");
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost("Verification")]
        public async Task<IActionResult> Verification([FromBody] LoginDto login)
        {
            try
            {
                User? user = await _context.Users.FirstOrDefaultAsync(x => x.MobileNumber == login.MobileNumber);
                if(user == null)
                {
                    return NotFound();
                }

               // var verificationCheck = VerificationCheckResource.Create(
               //    to: "+91" + login.MobileNumber,
               //    code: login.Otp,
               //    pathServiceSid: user.UserSid
               //);

               // if(verificationCheck == null)
               // {
               //     return BadRequest("OTP verification failed");
               // }
               // if(verificationCheck.Status == "approved")
               // {
                    return Ok(new { message = "OTP verification is successful.", isverified = true, userid = user.Id.ToString() });
                //} else
                //{
                //    return Ok(new { message = "OTP is incorrect.", isverified = false });
                //}

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
