﻿namespace SatifyApi.Models
{
    public class Cart
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public List<AddedProducts> AddedProducts { get; set; } = new();
    }
    public class AddedProducts
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public float Quantity { get; set; }
        public int CartId { get; set; }
        public Cart Cart { get; set; }
    }
}
