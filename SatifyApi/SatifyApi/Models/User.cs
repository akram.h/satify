﻿namespace SatifyApi.Models
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string MobileNumber { get; set; }
        public string Address { get; set; }
        public string UserRole { get; set; }
        public string? SubscriptionType { get; set; }
        public float Wallet { get; set; }
        public string? ProductsBought { get; set; }
        public string? CartItems { get; set; }
        public string? UserSid { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
    }
}
