﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SatifyApi.Models
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime OrderedDate { get; set; }
        List<PurchasedProduct> PurchasedProducts { get; set; }
        public string ShippingAddress { get; set; }
        public string Status { get; set; }
    }
    public class PurchasedProduct
    {
        public int OrderedId { get; set; }
        public int ProductId { get; set; }
        public float Quantity { get; set; }
    }
}
