﻿using Microsoft.EntityFrameworkCore;
using SatifyApi.Models;

namespace SatifyApi.Models
{
    public class SatifyDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<LoginModel> LoginModels { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<AddedProducts> AddedProducts { get; set; }
        public DbSet<Category> Category { get; set; } 
        public SatifyDbContext(DbContextOptions<SatifyDbContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AddedProducts>()
                .HasOne(c => c.Cart)
                .WithMany(p => p.AddedProducts)
                .HasForeignKey(c => c.CartId);

            base.OnModelCreating(modelBuilder);
        }
    }
}
