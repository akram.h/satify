﻿namespace SatifyApi.DTOs
{
    public class ProductDto
    {
        public string ProductName { get; set; } = string.Empty;
        public string ProductPrice { get; set; } = string.Empty;
        public float MaxQuantity { get; set; }
        public float MinQuantity { get; set; }
        public float Discount { get; set; }
        public string CategoryType { get; set; }
        public string ProductPath { get; set; }
        public int Rating { get; set; }
        public string? Description { get; set; }
    }
}
