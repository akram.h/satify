﻿namespace SatifyApi.DTOs
{
    public class UserDto
    {
        public string UserName { get; set; }
        public string? Email { get; set; }
        public string MobileNumber { get; set; }
        public string Address { get; set; }
        public string UserRole { get; set; }
        public string Password { get; set; }
        public string? SubscriptionType { get; set; }
        public float Wallet { get; set; }
    }
}
