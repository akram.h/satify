﻿namespace SatifyApi.DTOs
{
    public class AddedProductDto
    {
        public int ProductId { get; set; }
        public float Quantity { get; set; }
        public int CartId { get; set; }
    }
}
