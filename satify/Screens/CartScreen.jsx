import React, { useState } from 'react';
import { View, Text, FlatList, TouchableOpacity,ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import styles from '../components/cart/cartItem.style'; // Import styles from your CartItem.style.js file
import CartItem from '../components/cart/CartItem';

const sampleItems = [
  {
    id: '1',
    name: 'Product 1',
    image: 'https://via.placeholder.com/80',
    price: 20,
    discount: 8,
    quantity: 2,
    shippingCharges:0,
    gstAmount:0,
  },
  {
    id: '2',
    name: 'Product 2',
    image: 'https://via.placeholder.com/80',
    price: 30,
    discount: 5,
    quantity: 1,
    shippingCharges:0,
    gstAmount:0,
  },
  
  // Add more sample items here
];

const CartScreen = () => {
  const [cartItems, setCartItems] = useState(sampleItems);
  const navigation = useNavigation();

  const getTotalAmount = () => {
    return cartItems.reduce((total, item) => total + item.price * item.quantity, 0);
  };

  const getDiscountPrice = () => {
    const totalDiscountPrice = cartItems.reduce((total, item) => {
      const itemDiscount = item.discount * item.quantity;
      return total + itemDiscount;
    }, 0);
  
    return totalDiscountPrice;
  };
  
  const getGstAmount = () => {
  const totalGst = cartItems.reduce((total, item) => total + item.gstAmount, 0);
  return totalGst;
};

  const getShippingCharges = () => {
    const allItemsHaveZeroShippingCharges = cartItems.every(item => item.shippingCharges === 0);
  
    if (allItemsHaveZeroShippingCharges) {
      return 0;
    } else {
      const totalShippingCharges = cartItems.reduce((total, item) => total + item.shippingCharges, 0);
      return totalShippingCharges;
    }
  };
  
  const getPayableAmount = () => {
    const totalAmount = getTotalAmount();
    const discount = getDiscountPrice(cartItems);
    const gst = getGstAmount();
    const shippingCharges = getShippingCharges();
  
    const payableAmount = totalAmount - discount + gst + shippingCharges;
  
    return payableAmount;
  };

  const handleIncrease = (index) => {
    const updatedCartItems = [...cartItems];
    updatedCartItems[index].quantity++;
    setCartItems(updatedCartItems);
  };

  const handleDecrease = (index) => {
    const updatedCartItems = [...cartItems];
    if (updatedCartItems[index].quantity > 1) {
      updatedCartItems[index].quantity--;
      setCartItems(updatedCartItems);
    }
  };

  const handleRemove = (index) => {
    const updatedCartItems = cartItems.filter((_, i) => i !== index);
    setCartItems(updatedCartItems);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.cartHeading}>Your Cart</Text>
  
      <FlatList
        data={cartItems}
        keyExtractor={(item) => item.id}
        renderItem={({ item, index }) => (
          <CartItem
            item={item}
            onIncrease={() => handleIncrease(index)}
            onDecrease={() => handleDecrease(index)}
            onRemove={() => handleRemove(index)}
          />
        )}
      />
      <Text style={styles.orderSummaryHeading}>Order Summary</Text>

      <View style={styles.orderSummaryContainer}>
        <View style={styles.orderSummaryHeader}>
          <Text style={styles.orderSummaryHeaderItem}>Sl No.</Text>
          <Text style={styles.orderSummaryHeaderItem}>Item</Text>
          <Text style={styles.orderSummaryHeaderItem}>Price</Text>
          <Text style={styles.orderSummaryHeaderItem}>Offer Price</Text>
          <Text style={styles.orderSummaryHeaderItem}>Quantity</Text>
          <Text style={styles.orderSummaryHeaderItem}>Total</Text>
        </View>
        {cartItems.map((item, index) => (
          <View key={index} style={styles.orderSummaryItem}>
            <Text style={styles.orderSummaryItemText}>{index + 1}</Text>
            <Text style={styles.orderSummaryItemText}>{item.name}</Text>
            <Text style={styles.orderSummaryItemText}>Rs.{item.price}</Text>
            <Text style={styles.orderSummaryItemText}>Rs.{item.price - item.discount}</Text>
            <Text style={styles.orderSummaryItemText}>{item.quantity}</Text>
            <Text style={styles.orderSummaryItemText}>Rs.{(item.price-item.discount) * item.quantity}</Text>
          </View>
        ))}
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryLabel}>Total Price:</Text>
          <Text style={styles.summaryValue}>{getTotalAmount()}</Text>
        </View>
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryLabel}>Discount Price:</Text>
          <Text style={styles.summaryValue}>-{getDiscountPrice()}</Text>
        </View>
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryLabel}>GST</Text>
          <Text style={styles.summaryValue}>{getGstAmount()}</Text>
        </View>
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryLabel}>Shipping Charges</Text>
          <Text style={styles.summaryValue}>{getShippingCharges()}</Text>
        </View>
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryLabel}>Total Payable:</Text>
          <Text style={styles.summaryValue}>{getPayableAmount()}</Text>
        </View>
      </View>


      <View style={styles.buttonsContainer}>
        <View style={styles.buttonsInnerContainer}>
          <TouchableOpacity style={styles.addButton}>
            <Text style={styles.addButtonText}>Add More Items</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.paymentButton}
            onPress={() => navigation.navigate('OrderSummary')}
          >
            <Text style={styles.paymentButtonText}>Proceed for Payment</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default CartScreen;