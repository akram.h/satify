import React, { useState, useEffect } from "react";
import styles from "./style";
import {
    Alert,
    Keyboard,
    KeyboardAvoidingView,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    View,
    FlatList
} from "react-native";
import { Button, SocialIcon } from "react-native-elements";

// import * as Facebook from "expo-facebook";
import { useNavigation } from '@react-navigation/native';
import { fetchItems } from "../../data/Api";
import { login } from "../../data/Api";
import { verification } from "../../data/Api";
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function LoginScreen() {
    const navigation = useNavigation();
    const [condition, setCondition] = useState(true);

    const [mobileNumber, setMobileNumber] = useState("");
    const [otp, setOtp] = useState("");

    const UserLogin = async () => {
        try {
            const data = await login(mobileNumber);
            return data;
        } catch (error) {

        }
    }

    const handleMobileNumber = (mobileNumber) => {
        setMobileNumber(mobileNumber);
    };

    const handleOtp = (otp) => {
        setOtp(otp);
    };

    const onSignInPress = async () => {
        if (mobileNumber?.length !== 10) {
            Alert.alert("Warning", 'Please enter 10 digits of your mobile number.', [
                { text: 'OK' },
            ]);
            return false;
        }
        const response = await UserLogin();
        if (response === 404) {
            navigation.navigate("SignUp");
        }
        if (response) {
            setCondition(!condition);
            Alert.alert(response);
        }
        // UserLogin().then(() => {
        // });
    };

    const onLoginPress = async () => {
        const id = await AsyncStorage.getItem('userid');
        if (id) {
            navigation.navigate("Bottom Navigation");
        }
        const response = await verification(mobileNumber, otp);
        Alert.alert(response.message);
        if (response.isverified) {
            // Alert.alert(response.userid);
            await AsyncStorage.setItem('userid', response.userid);
            setMobileNumber("");
            setOtp("");
            setCondition(!condition);
            navigation.navigate("Bottom Navigation");
        } else {
            return false;
        }
    };

    const BackToSignIn = async () => {
        setCondition(!condition);
    };

    const onFbLoginPress = async () => {
        navigation.navigate("SignUp");
    };

    return (
        <KeyboardAvoidingView style={styles.containerView} behavior="padding">
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.loginScreenContainer}>
                    {condition ?
                        <View style={styles.loginFormView}>
                            <Text style={styles.logoText}>Satify</Text>
                            <TextInput
                                value={mobileNumber}
                                onChangeText={handleMobileNumber}
                                placeholder="Mobile Number"
                                placeholderColor="#c4c3cb"
                                style={styles.loginFormTextInput}
                                keyboardType="numeric"
                            />
                            <Button
                                buttonStyle={styles.loginButton}
                                onPress={() => onSignInPress()}
                                title="Login"
                            />
                            <Button
                                type="reset"
                                containerStyle={styles.fbLoginButton}
                                onPress={() => onFbLoginPress()}
                                title="Sign Up"
                            />
                        </View> :
                        <View style={styles.loginFormView}>
                            <Text style={styles.logoText}>Satify</Text>
                            <TextInput
                                value={otp}
                                onChangeText={handleOtp}
                                placeholder="Enter OTP"
                                placeholderColor="#c4c3cb"
                                style={styles.loginFormTextInput}
                            />
                            <Button
                                buttonStyle={styles.loginButton}
                                onPress={() => onLoginPress()}
                                title="Submit"
                            />
                            <Button
                                buttonStyle={styles.backButton}
                                onPress={() => BackToSignIn()}
                                title="Back"
                            />
                        </View>}
                </View>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    );
}