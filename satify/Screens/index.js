import Search from "./Search";
import Home from "./Home";
import Profile from "./Profile";
import CartScreen from "./CartScreen";
import ProductDetails from "./ProductDetails";
import Orders from "./Orders";
import Favourites from "./Favourites";
import Categories from "../components/categories/Categories";

export{
    Home,
    Search,
    Profile,
    CartScreen,
    ProductDetails,
    Orders,
    Favourites,
    Categories,
}