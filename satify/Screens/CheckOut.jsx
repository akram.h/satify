import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';
import { COLORS } from '../constants';

class CheckOut extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cartItems: [
        {
          id: 1,
          name: 'Item 1',
          price: 100,
          quantity: 2,
          imageSource: require('../assets/images/Vegetables.jpg'), // Replace with actual image source
        },
        // Add more items here
      ],
    };
  }

  render() {
    const { cartItems } = this.state;

    const subtotal = cartItems.reduce((total, item) => total + item.price * item.quantity, 0);
    const discount = 10; // Replace with actual discount value
    const shippingCharges = 5; // Replace with actual shipping charges
    const handlingCharges = 2; // Replace with actual handling charges
    const totalPayable = subtotal - discount + shippingCharges + handlingCharges;

    return (
      <View style={styles.container}>
        <Text style={styles.heading}>Order Summary</Text>
        <ScrollView>
          {cartItems.map((item) => (
            <View key={item.id} style={styles.itemContainer}>
              <Text style={styles.itemName}>{item.name}</Text>
              <Text style={styles.itemPrice}>${item.price}</Text>
            </View>
          ))}
        </ScrollView>
        {/* Display summary details */}
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryLabel}>Subtotal:</Text>
          <Text style={styles.summaryValue}>${subtotal}</Text>
        </View>
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryLabel}>Discount:</Text>
          <Text style={styles.summaryValue}>-${discount}</Text>
        </View>
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryLabel}>Shipping Charges:</Text>
          <Text style={styles.summaryValue}>${shippingCharges}</Text>
        </View>
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryLabel}>Handling Charges:</Text>
          <Text style={styles.summaryValue}>${handlingCharges}</Text>
        </View>
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryLabel}>Total Payable:</Text>
          <Text style={styles.summaryValue}>${totalPayable}</Text>
        </View>
        <TouchableOpacity onPress={() => {}} style={styles.confirmButton}>
          <Text style={styles.buttonText}>Proceed to Payment</Text>
        </TouchableOpacity>
      </View>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F7F7F7',
    padding: 16,
  },
  heading: {
    marginTop:20,
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  summaryContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 10,
    borderTopWidth: 1,
    borderTopColor: '#ccc',
    marginBottom: 10,
  },
  summaryLabel: {
    fontSize: 16,
  },
  summaryValue: {
    fontSize: 16,
    color: COLORS.primary,
    fontWeight: 'bold',
  },
  buttonContainer: {
    alignItems: 'center',
    marginTop: 20,
  },
  confirmButton: {
    backgroundColor: COLORS.primary,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 8,
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default CheckOut;
