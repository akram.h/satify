import { TextInput, TouchableOpacity, View, Alert } from 'react-native'
import React, { useState, useEffect } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { COLORS, SIZES } from '../constants';
import styles from './search.style';
import { Feather, Ionicons } from '@expo/vector-icons';

const Search = () => {
  const [searchValue, setSearchValue] = useState("")

  const handleSearchValue = (search) => {
    setSearchValue(search);
  };

  const filterProducts = () => {
    Alert.alert("Works");
  }

  return (
    <SafeAreaView>
      <View style={styles.searchContainer}>

        <TouchableOpacity>
          <Ionicons name="camera-outline" size={SIZES.xLarge} style={styles.searchIcon} />
        </TouchableOpacity>

        <View style={styles.searchWrapper}>
          <TextInput style={styles.searchInput}
            value={searchValue}
            onChangeText={handleSearchValue}
            placeholder="What are you looking for"
          />
        </View>

        <View>
          <TouchableOpacity style={styles.searchBtn}
            onPress={filterProducts}
          >
            <Feather name="search" size={24}
              color={COLORS.offwhite} />
          </TouchableOpacity>
        </View>

      </View>
    </SafeAreaView>
  )
}

export default Search;

