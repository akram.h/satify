import React, { useState, useEffect } from "react";
import styles from "./style";
import {
    Alert,
    Keyboard,
    KeyboardAvoidingView,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    View,
} from "react-native";
import { Button, SocialIcon } from "react-native-elements";

// import * as Facebook from "expo-facebook";
import { useNavigation } from '@react-navigation/native';
// import DateTimePickerModal from "react-native-modal-datetime-picker";
import { register } from "../../data/Api";

const appId = "1047121222092614";

export default function SignUpScreen() {
    const navigation = useNavigation();
    const onLoginPress = async () => {
        if (userName == "" || userAddress == "" || mobileNumber == "") {
            Alert.alert('Oops...', 'Please fill all the fields', [
                // {
                //     text: 'Ask me later',
                //     onPress: () => console.log('Ask me later pressed'),
                // },
                // {
                //     text: 'Cancel',
                //     onPress: () => console.log('Cancel Pressed'),
                //     style: 'cancel',
                // },
                { text: 'OK' },
            ]);
            return;
        }
        try {
            const response = await register(userName, userAddress, mobileNumber);
            if (!response) {
                return;
            }
            // Alert.alert("Register", response);
            setUserName("");
            setUserAddress("");
            // setPassword("");
            // setConfirmPassword("");
            setMobileNumber("");
            navigation.navigate("Login");

        } catch (error) {
            console.warn(error);
        }
    };

    const [userName, setUserName] = useState("");
    const [userAddress, setUserAddress] = useState("");
    // const [userDateOfBirth, setUserDateOfBirth] = useState(new Date());
    // const [passWord, setPassword] = useState("");
    // const [confirmPassWord, setConfirmPassword] = useState("");
    const [mobileNumber, setMobileNumber] = useState("");
    // const [passwordError, setPasswordError] = useState("");

    // const [passwordMatch, setPasswordMatchCondition] = useState(true);

    const onResetPress = async () => {
        setUserName("");
        setUserAddress("");
        // setPassword("");
        // setConfirmPassword("");
        setMobileNumber("");
    };

    const handleUserName = (userName) => {
        setUserName(userName);
    };

    const handleUserAddress = (userAddress) => {
        setUserAddress(userAddress);
    };

    // const handleUserDateOfBirth = (userDateOfBirth) => {
    //     setUserAddress(userDateOfBirth);
    // };

    // const handlePassword = (passWord) => {
    //     setPassword(passWord);
    // };

    // const handleConfirmPassword = (confirmPassWord) => {
    //     setConfirmPassword(confirmPassWord);
    //     if (confirmPassWord !== passWord) {
    //         setPasswordError('Passwords not matching.');
    //         setPasswordMatchCondition(passwordMatch);
    //     }
    // };

    const handleMobileNumber = (mobileNumber) => {
        setMobileNumber(mobileNumber);
    };

    // const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    // const showDatePicker = () => {
    //     setDatePickerVisibility(true);
    // };

    // const hideDatePicker = () => {
    //     setDatePickerVisibility(false);
    // };

    // const handleConfirm = (date) => {
    //     // console.warn("A date has been picked: ", date);
    //     setUserDateOfBirth(date);
    //     hideDatePicker();
    // };

    return (
        <KeyboardAvoidingView style={styles.containerView} behavior="padding">
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.loginScreenContainer}>
                    <View style={styles.loginFormView}>
                        <Text style={styles.logoText}>Satify</Text>
                        <TextInput
                            value={userName}
                            onChangeText={handleUserName}
                            placeholder="Name"
                            placeholderColor="#c4c3cb"
                            style={styles.loginFormTextInput}
                        />
                        <TextInput
                            value={userAddress}
                            onChangeText={handleUserAddress}
                            placeholder="Address"
                            placeholderColor="#c4c3cb"
                            style={styles.loginFormTextInput}
                        />
                        {/* <Button title="Select Date of Birth" onPress={showDatePicker} /> */}
                        {/* <DateTimePickerModal
                            isVisible={isDatePickerVisible}
                            mode="date"
                            onConfirm={handleConfirm}
                            onCancel={hideDatePicker}
                        /> */}
                        {/* <TextInput
                            value={passWord}
                            onChangeText={handlePassword}
                            placeholder="Password"
                            placeholderColor="#c4c3cb"
                            style={styles.loginFormTextInput}
                            secureTextEntry={true}
                        />
                        <TextInput
                            value={confirmPassWord}
                            onChangeText={handleConfirmPassword}
                            placeholder="Confirm Password"
                            placeholderColor="#c4c3cb"
                            style={styles.loginFormTextInput}
                            secureTextEntry={true}
                        />
                        {
                            passwordMatch ? <Text style={styles.errorText}>{passwordError}</Text> : null
                        } */}
                        <TextInput
                            value={mobileNumber}
                            onChangeText={handleMobileNumber}
                            placeholder="Mobile Number"
                            placeholderColor="#c4c3cb"
                            style={styles.loginFormTextInput}
                            keyboardType="numeric"
                        />
                        <Button
                            buttonStyle={styles.loginButton}
                            onPress={() => onLoginPress()}
                            title="Register"
                        />
                        <Button
                            containerStyle={styles.fbLoginButton}
                            type="clear"
                            onPress={() => onResetPress()}
                            title="Reset"
                        />
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    );
}
