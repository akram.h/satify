import { StyleSheet, TouchableOpacity, ViewStyle } from "react-native";
import { COLORS, SIZES } from "../constants";
import { Feather, Ionicons } from '@expo/vector-icons';


const styles = StyleSheet.create({
    searchContainer:{
        flexDirection: "row",
        justifyContent:"center",
        alignItems:"center",
        alignContent:"center",
        backgroundColor:COLORS.secondary,
        borderRadius:SIZES.medium,
        marginVertical:SIZES.medium,
        height:50
    },
    searchIcon:{
        marginHorizontal: 10,
        color: COLORS.gray
    },
    searchWrapper:{
        flex:1,
        backgroundColor:COLORS.secondary,
        marginRight:SIZES.small,
        borderRadius:SIZES.small
    }, 
    searchInput:{
        fontFamily:"regular",
        width:"100%",
        height:"100%",
        paddingHorizontal:SIZES.xSmall
    },
    searchBtn:{
        width:50,
        height:50,
        backgroundColor:COLORS.primary,
        borderRadius:SIZES.medium,
        justifyContent:"center",
        alignItems:"center",
        marginVertical: SIZES.medium,
        marginTop: SIZES.small

    }
});

export default styles