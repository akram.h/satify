import { StyleSheet, Text, View, Image, TouchableOpacity, Alert } from 'react-native'
import React, { useState, useEffect } from 'react';
import style from './profile.style';
import { StatusBar } from 'expo-status-bar';
import { COLORS } from '../constants';
import { AntDesign, MaterialCommunityIcons, SimpleLineIcons } from '@expo/vector-icons';

import profile from '../assets/images/profile.jpeg';
import space from '../assets/images/space.jpg';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { userInfoo } from '../data/Api';

const Profile = ({ navigation }) => {
  const [userData, setUserData] = useState(null)
  const [userLogin, setUserLogin] = useState(true)

  useEffect(() => {
    getUserInformation();
  }, []);

  const getUserInformation = async () => {
    // Alert.alert("Working");
    const id = await AsyncStorage.getItem('userid');
    console.log(id);
    const user = await userInfoo(id);
    setUserData(user);
  };

  const logout = () => {
    Alert.alert(
      "Logout",
      "Are you sure you want to logout",
      [
        {
          text: "Cancel", onPress: () => { }
        },
        {
          text: "Continue", onPress: async () => {
            await AsyncStorage.clear();
            navigation.navigate("Login");
          }
        }
      ]
    )
  }

  return (
    <View style={style.container}>
      <View style={style.container}>
        <StatusBar backgroundColor={COLORS.gray} />

        <View style={{ width: '100%' }}>
          <Image
            source={space}
            style={style.cover}
          />
        </View>
        <View style={style.profileContainer}>
          <Image
            source={profile}
            style={style.profile}
          />
          <Text style={style.name}>
            {userLogin === true && userData ? userData.UserName : ""}
          </Text>
          {userLogin === false ? (
            <TouchableOpacity onPress={() => { }}>
              <View style={style.loginBtn}>
                <Text style={style.menutext}> </Text>
              </View>
            </TouchableOpacity>
          ) : (
            <View style={style.loginBtn}>
              <Text style={style.menutext}> {userData ? userData.MobileNumber : null}</Text>
            </View>
          )}

          <Text style={style.name}>
            {userLogin === true && userData ? userData.Address : ""}
          </Text>

          {userLogin === false ? (
            <View></View>
          ) : (
            <View style={style.menuWrapper}>
              <TouchableOpacity onPress={() => navigation.navigate('Favourites')}>
                <View style={style.menuItem(0.2)}>
                  <MaterialCommunityIcons
                    name="heart-outline"
                    color={COLORS.primary}
                    size={24}
                  />
                  <Text style={style.menutext}>Favourites</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => navigation.navigate('Orders')}>
                <View style={style.menuItem(0.2)}>
                  <MaterialCommunityIcons
                    name="truck-delivery-outline"
                    color={COLORS.primary}
                    size={24}
                  />
                  <Text style={style.menutext}>Orders</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => navigation.navigate('Cart')}>
                <View style={style.menuItem(0.2)}>
                  <SimpleLineIcons
                    name="bag"
                    color={COLORS.primary}
                    size={24}
                  />
                  <Text style={style.menutext}>Cart</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => { }}>
                <View style={style.menuItem(0.2)}>
                  <MaterialCommunityIcons
                    name="wallet"
                    color={COLORS.primary}
                    size={24}
                  />
                  <Text style={style.menutext}>Wallet</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => logout()}>
                <View style={style.menuItem(0.2)}>
                  <AntDesign
                    name="logout"
                    color={COLORS.primary}
                    size={24}
                  />
                  <Text style={style.menutext}>Logout</Text>
                </View>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>

    </View>
  )
}

export default Profile

