import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useFonts } from 'expo-font';
import * as SplashScreen from "expo-splash-screen";
import { useCallback } from 'react';
import BottomTabNavigation from './navigation/BottomTabNavigation';
import { CartScreen, ProductDetails, Orders, Favourites, Categories, Profile } from './Screens';
import OrderSummary from './components/checkout/OrderSummary';
import LoginScreen from './Screens/Login/Login';
import SignUpScreen from './Screens/SignUp/SignUp';
import AdminHomeScreen from './Screens/Admin/Home/AdminHomeScreen';

const Stack = createNativeStackNavigator();

export default function App() {
  const [fontsLoaded] = useFonts({
    regular: require("./assets/fonts/Poppins-Regular.ttf"),
    bold: require("./assets/fonts/Poppins-Bold.ttf"),
    extrabold: require("./assets/fonts/Poppins-ExtraBold.ttf"),
    light: require("./assets/fonts/Poppins-Light.ttf"),
    medium: require("./assets/fonts/Poppins-Medium.ttf"),
    semibold: require("./assets/fonts/Poppins-SemiBold.ttf"),
  })

  const onLayoutRootView = useCallback(async () => {
    if (fontsLoaded) {
      await SplashScreen.hideAsync();
    }
  }, [fontsLoaded]);

  if (!fontsLoaded) {
    return null;
  }
  return (
    <NavigationContainer>
      <Stack.Navigator>

        <Stack.Screen name='Login' component={LoginScreen} />

        <Stack.Screen name='SignUp' component={SignUpScreen} />

        <Stack.Screen name='AdminHome' component={AdminHomeScreen} />

        <Stack.Screen name='Profile' component={Profile} />

        <Stack.Screen
          name='Bottom Navigation'
          component={BottomTabNavigation}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name='CartScreen'
          component={CartScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name='ProductDetails'
          component={ProductDetails}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name='Orders'
          component={Orders}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name='Favourites'
          component={Favourites}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name='Categories'
          component={Categories}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="OrderSummary"
          component={OrderSummary}
          options={{ headerShown: false }}
        />

      </Stack.Navigator>
    </NavigationContainer>
  );
}

