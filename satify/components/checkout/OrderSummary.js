import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import { COLORS, SIZES } from '../../constants';

class OrderSummary extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cartItems: [
        {
          id: 1,
          name: 'Item 1',
          price: 100,
          quantity: 2,
          imageSource: require('../../assets/images/Vegetables.jpg'), // Replace with actual image source
        },
        // Add more items here
      ],
    };
  }

  render() {
    const { cartItems } = this.state;

    const subtotal = cartItems.reduce((total, item) => total + item.price * item.quantity, 0);
    const discount = 10; // Replace with actual discount value
    const shippingCharges = 5; // Replace with actual shipping charges
    const handlingCharges = 2; // Replace with actual handling charges
    const totalPayable = subtotal - discount + shippingCharges + handlingCharges;

    const renderCartItem = ({ item }) => (
      <View style={styles.itemContainer}>
        <Text style={styles.itemName}>{item.name}</Text>
        <Text style={styles.itemPrice}>MRP: Rs.{item.price}</Text>
        <Text style={styles.itemDiscount}>Discount: Rs.{discount}</Text>
        <Text style={styles.itemQuantity}>Quantity: {item.quantity}</Text>
        <Text style={styles.itemTotal}>Total: Rs.{item.price * item.quantity}</Text>
      </View>
    );

    return (
      <View style={styles.container}>
        <Text style={styles.heading}>Order Summary</Text>
        <FlatList
          data={cartItems}
          renderItem={renderCartItem}
          keyExtractor={(item) => item.id.toString()}
          contentContainerStyle={styles.itemList}
        />
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryLabel}>Subtotal:</Text>
          <Text style={styles.summaryValue}>Rs.{subtotal}</Text>
        </View>
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryLabel}>Discount:</Text>
          <Text style={styles.summaryValue}>-Rs.{discount}</Text>
        </View>
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryLabel}>Shipping Charges:</Text>
          <Text style={styles.summaryValue}>Rs.{shippingCharges}</Text>
        </View>
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryLabel}>Handling Charges:</Text>
          <Text style={styles.summaryValue}>Rs.{handlingCharges}</Text>
        </View>
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryLabel}>Total Payable:</Text>
          <Text style={styles.summaryValue}>Rs.{totalPayable}</Text>
        </View>
        <TouchableOpacity onPress={() => {}} style={styles.payButton}>
          <Text style={styles.buttonText}>Proceed to Payment</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F7F7F7',
    padding: 16,
  },
  heading: {
    marginTop: 20,
    fontSize: SIZES.xLarge,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  itemList: {
    marginTop: 10,
  },
  itemContainer: {
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    paddingVertical: 12,
    marginBottom: 8,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 3,
  },
  itemName: {
    fontSize: SIZES.medium,
    fontWeight: 'bold',
    marginBottom: 4,
  },
  itemPrice: {
    fontSize: SIZES.small,
  },
  itemDiscount: {
    fontSize: SIZES.small,
  },
  itemQuantity: {
    fontSize: SIZES.small,
  },
  itemTotal: {
    fontSize: SIZES.small,
  },
  summaryContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 8,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  summaryLabel: {
    fontSize: SIZES.medium,
  },
  summaryValue: {
    fontSize: SIZES.medium,
    color: COLORS.primary,
    fontWeight: 'bold',
  },
  payButton: {
    backgroundColor: COLORS.primary,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 8,
    marginTop: 16,
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default OrderSummary;
