import React from 'react';
import { TouchableOpacity, Image, Text, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const CategoryItem = ({ id, name, imageSource }) => {
  const navigation = useNavigation();

  const handleCategoryPress = () => {
    // Navigate to the specific category page with id
    navigation.navigate('CategoryDetail', { categoryId: id });
  };

  return (
    <TouchableOpacity style={styles.container}>
      <Image source={imageSource} style={styles.image} />
      <Text style={styles.categoryName}>{name}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '45%',
    aspectRatio: 1,
    backgroundColor: '#FFF',
    borderRadius: 8,
    margin: 8,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 3,
  },
  image: {
    width: '70%',
    height: '70%',
    resizeMode: 'cover',
  },
  categoryName: {
    marginTop: 8,
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default CategoryItem;
