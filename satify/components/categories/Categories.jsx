import React, { useState } from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity, Text } from 'react-native';
import CategoryItem from './CategoryItem';
import Search from '../../Screens/Search';
import { COLORS } from '../../constants';

const categoriesData = [
  { id: '1', name: 'Vegetables', imageSource: require('../../assets/images/Vegetables.jpg') },
  { id: '2', name: 'Chicken & Meat', imageSource: require('../../assets/images/Chicken_Meat.jpg') },
  { id: '3', name: 'Fruits', imageSource: require('../../assets/images/Fruits.jpg') },
  // Add more categories
];

const Categories = () => {
  const [numColumns, setNumColumns] = useState(2); // Initial number of columns

  const toggleColumns = () => {
    setNumColumns(numColumns === 2 ? 1 : 2); // Toggle between 2 and 1 columns
  };

  const renderItem = ({ item }) => (
    <CategoryItem id={item.id} name={item.name} imageSource={item.imageSource} />
  );

  return (
    <View style={styles.container}>
      <Search />
      <View>
        <Text>Categories</Text>
      </View>
      <FlatList
        data={categoriesData}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        numColumns={numColumns}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.lightWhite,
    paddingVertical: 10,
  },
});

export default Categories;
