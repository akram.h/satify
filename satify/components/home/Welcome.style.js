import { StyleSheet } from "react-native";
import { COLORS, SIZES } from "../../constants";

const styles = StyleSheet.create({
    container:{
        width:"100%"
    },
    welcomeTxt: (color,top)=>({
        fontFamily: "bold",
        fontSize: SIZES.xLarge -2,
        marginTop: top,
        color: color,
        marginHorizontal: 12
    }),
    searchContainer:{
        flexDirection: "row",
        justifyContent:"center",
        alignItems:"center",
        alignContent:"center",
        backgroundColor:COLORS.secondary,
        borderRadius:SIZES.medium,
        marginVertical:SIZES.medium,
        height:50
    },
    searchIcon:{
        marginHorizontal: 10,
        color: COLORS.gray
    },
    searchWrapper:{
        flex:1,
        backgroundColor:COLORS.secondary,
        marginRight:SIZES.small,
        borderRadius:SIZES.small
    }, 
    searchInput:{
        fontFamily:"regular",
        width:"100%",
        height:"100%",
        paddingHorizontal:SIZES.xSmall
    },
    searchBtn:{
        width:40,
        height:40,
        backgroundColor:COLORS.primary,
        borderRadius:SIZES.medium,
        justifyContent:"center",
        alignItems:"center",
        marginVertical: SIZES.medium,
        marginTop: SIZES.small

    }

    
})
export default styles