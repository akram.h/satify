import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import styles from './cartItem.style';
import { ScrollView } from 'react-native-gesture-handler';

const CartItem = ({ item, onIncrease, onDecrease, onRemove }) => {
  const discountedPrice = item.price - item.discount;

  return (
    <ScrollView>
    <View style={styles.cartItem}>
      <Image source={{ uri: item.image }} style={styles.itemImage} />
      
      <View style={styles.itemDetails}>
      <Text style={styles.itemName}>{item.name}</Text>

      <View style={styles.priceAndQuantityContainer}>
          <View>
            <Text style={styles.itemPrice}>Price: {item.price}</Text>
            <Text style={styles.itemDiscount}>Offer Price: {discountedPrice}</Text>
          </View>
          
          <View style={styles.quantityControl}>
            <Text style={styles.quantity}>Qty.: </Text>
            <TouchableOpacity onPress={onDecrease}>
              <Icon name="minus" size={18} color="#333" />
            </TouchableOpacity>
            <Text style={styles.quantity}>{item.quantity}</Text>
            <TouchableOpacity onPress={onIncrease}>
              <Icon name="plus" size={20} color="#333" />
            </TouchableOpacity>
          </View>
        </View>

        <Text style={styles.itemTotal}>Total: Rs.{discountedPrice * item.quantity}</Text>
      </View>

      <TouchableOpacity onPress={onRemove}>
        <Icon name="trash" size={20} color="#FF0000" />
      </TouchableOpacity>
    </View>
    </ScrollView>
  );
};

export default CartItem;
