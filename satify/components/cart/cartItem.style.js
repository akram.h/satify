import { StyleSheet } from 'react-native';
import { COLORS, SIZES } from '../../constants';

const styles = StyleSheet.create({
    cartItem: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
      },
      priceDiscountContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5,
      },
      cartHeading: {
        fontSize: SIZES.xLarge,
        fontWeight: 'bold',
        marginBottom: 10,
        marginTop:30,
      },
      itemImage: {
        width: 80,
        height: 80,
        marginRight: 10,
        resizeMode: 'cover',
      },
      priceAndQuantityContainer: {
        flexDirection: 'row', // To place price and quantity side by side
        justifyContent: 'space-between', // To evenly space the price and quantity
        alignItems: 'center', // To vertically align the content
        marginTop: 8, // Adjust the spacing as needed
      },
      itemDetails: {
        flex: 1,
      },
      itemName: {
        fontSize: 16,
        fontWeight: 'bold',
      },
      itemPrice: {
        fontSize: SIZES.small,
        color: '#333',
        marginBottom: 5,
        textDecorationLine: 'line-through',
      },
      quantity: {
        fontSize: SIZES.medium,
        color: '#333',
        marginHorizontal:10,
      },
      itemDiscount: {
        fontSize: 14,
        color: 'green',
        marginBottom: 5,
      },
      quantityControl: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5,
      },
      
      itemTotal: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#333',
      },
      removeButton: {
        backgroundColor: COLORS.red,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderRadius: 4,
        color: COLORS.white,
        marginTop:10,
        marginRight:10,
        alignContent:'center'
      },
      addButton: {
        backgroundColor: '#4CAF50',
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 5,
        marginRight: 10,
      },
      addButtonText: {
        color: '#fff',
        fontSize: 14,
        fontWeight: 'bold',
      },

    buttonsContainer: {
    marginTop: 20,
    marginLeft:10,
    marginRight:10,
  },
  buttonsInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center', // Optional: Align buttons vertically centered
  },
  
  addButton: {
    backgroundColor: COLORS.primary,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 8,
  },
  addButtonText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  paymentButton: {
    backgroundColor: COLORS.primary,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 8,
  },
  paymentButtonText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  orderSummaryContainer: {
    marginTop: 20,
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 3,
  },
  orderSummaryHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  orderSummaryHeaderText: {
    fontSize: SIZES.small,
    fontWeight: 'bold',
  },
  orderSummaryHeaderItem: {
    flex: 1,
    fontSize: SIZES.small,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  orderSummaryItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  summaryContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 8,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    marginLeft:20,
    marginRight:20,
  },
  summaryLabel: {
    fontSize: SIZES.small,
  },
  summaryValue: {
    fontSize: SIZES.small,
    color: COLORS.primary,
    fontWeight: 'bold',
  },
  orderSummaryHeading: {
    fontSize: SIZES.large,
    fontWeight: 'bold',
    textAlign:'center',
    marginVertical: 10,
  
  },
  orderSummaryItemText: {
    flex: 1,
    fontSize: SIZES.small,
    textAlign: 'center',
  },
  orderSummaryRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  orderSummaryRowText: {
    fontSize: SIZES.small,
  },
  orderSummaryFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    paddingVertical: 10,
    borderTopWidth: 1,
    borderTopColor: '#ccc',
  },
  orderSummaryFooterText: {
    fontSize: SIZES.medium,
    fontWeight: 'bold',
    color: COLORS.primary,
  },
    
    
});

export default styles;