import axios from 'axios';
import { Alert } from 'react-native-web';

const BASE_URL = 'http://192.168.1.4:5261/api/';

const api = axios.create({
    baseURL: BASE_URL,
});

export const fetchItems = async (name) => {
    try {
        const response = await api.get(name);
        return response.data;
    } catch (error) {
        console.error('Error fetching items:', error);
        throw error;
    }
};

export const userInfoo = async (id) => {
    try {
        const response = await api.get('Users/' + parseInt(id));
        console.log('Users/' + id);
        console.log(response);
        return response.data;
    } catch (error) {
        console.log(error);
        throw error;
    }
};

export const login = async (MobileNumber) => {
    try {
        const response = await api.post('Auth/Login', {
            MobileNumber
        });
        if (response.status === 200) {
            return response.data;
        } else {
            if (response.status === 404) {
                Alert.alert("Failed", "User Not Reigstered. Please register with your Mobile Number.", [
                    { text: 'OK' }
                ]);
                return response.status;
            } else {
                Alert.alert("Oops...", "Something went wrong", [
                    { text: 'OK' }
                ]);
                return false;
            }
        }
    } catch (error) {
        console.error(error);
        throw error;
    }
};

export const register = async (UserName, Address, Mobilenumber) => {
    try {
        const response = await api.post('Auth/Register', {
            UserName,
            Address,
            Mobilenumber
        });
        if (response.status === 200) {
            return response.data;
        } else {
            Alert.alert('Oops...', 'Failed to register.', [
                { text: 'OK' }
            ]);
            return false;
        }
    } catch (error) {
        console.warn(error);
    }
};

export const verification = async (MobileNumber, Otp) => {
    try {
        const response = await api.post('Auth/Verification', {
            MobileNumber,
            Otp
        });
        if (response.status === 200) {
            return response.data;
        } else {
            Alert.alert('Oops...', 'Something went wrong.', [
                { text: 'OK' }
            ]);
            return false;
        }
    } catch (error) {

    }
}